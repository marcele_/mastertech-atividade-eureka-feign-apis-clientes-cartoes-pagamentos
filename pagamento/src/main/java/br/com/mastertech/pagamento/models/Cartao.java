package br.com.mastertech.pagamento.models;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class Cartao {

    private int id;
    private String numero;
    private int clienteId;
    private boolean ativo;

    public Cartao() {
    }

    public Cartao(int id, String numero, int clienteId, boolean ativo) {
        this.id = id;
        this.numero = numero;
        this.clienteId = clienteId;
        this.ativo = ativo;
    }

    public Cartao(@NotNull @NotBlank String numero, int clienteId) {
        this.numero = numero;
        this.clienteId = clienteId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
