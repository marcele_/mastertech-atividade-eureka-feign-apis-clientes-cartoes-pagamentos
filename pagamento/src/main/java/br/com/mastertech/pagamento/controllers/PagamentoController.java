package br.com.mastertech.pagamento.controllers;

import br.com.mastertech.pagamento.models.Pagamento;
import br.com.mastertech.pagamento.security.principal.Usuario;
import br.com.mastertech.pagamento.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
public class PagamentoController {

    @Autowired
    PagamentoService pagamentoService;

    @GetMapping("/pagamentos/{id_cartao}")
    public List<Pagamento> buscarPorIdCartao(@Valid @PathVariable(name = "id_cartao") int idCartao){
        //try{
            return pagamentoService.buscarPorIdCartao(idCartao);
        //}catch (Exception ex){
            //throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        //}
    }

    @PostMapping("/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Pagamento> cadastrar(@RequestBody @Valid Pagamento pagamento,
                                               @AuthenticationPrincipal Usuario usuario){
        //try{
            return ResponseEntity.status(201).body(pagamentoService.salvar(pagamento));
        //}catch (Exception ex){
          //  throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        //}
    }
}

