package br.com.mastertech.pagamento.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Pagamento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "A descrição da compra não pode ser null.")
    @NotBlank(message = "A descrição da compra não pode estar em branco.")
    @Size(min = 2, max = 400, message = "A descrição da compra deve ter no mínimo 2 caracteres. ")
    @Column
    private String descricao;

    @NotNull
    @Column
    private double valor;

    @Column
    @JsonProperty("cartao_id")
    private int idCartao;

    public Pagamento() {
    }

    public Pagamento(@NotNull(message = "A descrição da compra não pode ser null.") @NotBlank(message = "A descrição da compra não pode estar em branco.") @Size(min = 2, max = 400, message = "A descrição da compra deve ter no mínimo 2 caracteres. ") String descricao, @NotNull double valor, int idCartao) {
        this.descricao = descricao;
        this.valor = valor;
        this.idCartao = idCartao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getIdCartao() {
        return idCartao;
    }

    public void setIdCartao(int idCartao) {
        this.idCartao = idCartao;
    }
}

