package br.com.mastertech.pagamento.services;

import br.com.mastertech.pagamento.clients.CartaoClient;
import br.com.mastertech.pagamento.models.Cartao;
import br.com.mastertech.pagamento.models.Pagamento;
import br.com.mastertech.pagamento.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoClient cartaoClient;

    @NewSpan("consulta-pagamento-por-cartao")
    public List<Pagamento> buscarPorIdCartao(@SpanTag(key = "idCartao") int idCartao){
        Optional<Cartao> cartaoOptional = cartaoClient.buscarPorId(idCartao);

        if (cartaoOptional.isPresent()){
            return pagamentoRepository.findAllByIdCartao(cartaoOptional.get().getId());
        } else {
            throw new RuntimeException("O cartão "+ idCartao+ " não foi encontrado!");
        }
    }

    public Pagamento salvar(Pagamento pagamento){
        Optional<Cartao> cartaoOptional = cartaoClient.buscarPorId(pagamento.getIdCartao());

        if (cartaoOptional.isPresent()){
            return pagamentoRepository.save(pagamento);
        }
        else
        {
            throw new RuntimeException("O cartão "+pagamento.getIdCartao()+" não foi encontrado e o pagamento não foi realizado!");
        }
    }

}
