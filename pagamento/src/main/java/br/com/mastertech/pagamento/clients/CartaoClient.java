package br.com.mastertech.pagamento.clients;

import br.com.mastertech.pagamento.models.Cartao;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.validation.Valid;
import java.util.Optional;

//@FeignClient(name = "cartao", url = "http://localhost:8082/cartao/")
//@FeignClient(name = "cartao")
@FeignClient(name = "cartao", configuration = CartaoClientConfiguration.class)
public interface CartaoClient {

    @GetMapping("/cartao/{id}")
    Optional<Cartao> buscarPorId(@Valid @PathVariable(name = "id") int id);
}
