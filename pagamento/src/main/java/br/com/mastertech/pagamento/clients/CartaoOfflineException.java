package br.com.mastertech.pagamento.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "O serviço de cartão se encontra offline!")
public class CartaoOfflineException extends RuntimeException{
}
