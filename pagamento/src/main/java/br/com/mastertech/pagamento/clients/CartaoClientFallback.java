package br.com.mastertech.pagamento.clients;

import br.com.mastertech.pagamento.models.Cartao;

import javax.validation.Valid;
import java.util.Optional;

public class CartaoClientFallback implements CartaoClient{

    @Override
    public Optional<Cartao> buscarPorId(@Valid int id) {
        throw new CartaoOfflineException();
    }

}
