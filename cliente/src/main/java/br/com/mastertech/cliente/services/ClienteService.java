package br.com.mastertech.cliente.services;

import br.com.mastertech.cliente.exceptions.ClienteNotFoundException;
import br.com.mastertech.cliente.models.Cliente;
import br.com.mastertech.cliente.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Iterable<Cliente> listarTodos(){
        return clienteRepository.findAll();
    }

    public Cliente buscarPorId(int id){
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);

        if (clienteOptional.isPresent()){
            return clienteOptional.get();
        } else {
            throw new ClienteNotFoundException();
        }
    }

    public Cliente salvar(Cliente cliente){
        return clienteRepository.save(cliente);
    }

}
