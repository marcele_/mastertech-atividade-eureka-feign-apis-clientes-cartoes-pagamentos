package br.com.mastertech.cliente.controllers;

import br.com.mastertech.cliente.exceptions.ClienteNotFoundException;
import br.com.mastertech.cliente.models.Cliente;
import br.com.mastertech.cliente.security.principal.Usuario;
import br.com.mastertech.cliente.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @GetMapping("/clientes")
    public Iterable<Cliente> listar() {
        try {
            System.out.println("CLIENTE recebeu uma requisição para LISTAR Clientes! ");
            return clienteService.listarTodos();
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/cliente/{id}")
    public Cliente buscarPorId(@Valid @PathVariable(name = "id") int id) {
        try {
            System.out.println("CLIENTE recebeu uma requisição para PROCURAR o cliente: " + id);
            return clienteService.buscarPorId(id);
        } catch (ClienteNotFoundException ex) {
            throw new ClienteNotFoundException();
        }
    }

    @PostMapping("/cliente")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Cliente> cadastrar(@RequestBody @Valid Cliente cliente,
                                             @AuthenticationPrincipal Usuario usuario) {
        try {
            System.out.println("CLIENTE recebeu uma requisição para INCLUIR o cliente.");
            return ResponseEntity.status(201).body(clienteService.salvar(cliente));
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());

        }
    }
}
