package br.com.mastertech.cartao.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "O serviço de cliente se encontra offline!")
public class ClienteOfflineException extends RuntimeException {
}
