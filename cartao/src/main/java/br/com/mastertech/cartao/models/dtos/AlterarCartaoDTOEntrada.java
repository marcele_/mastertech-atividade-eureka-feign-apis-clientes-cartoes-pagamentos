package br.com.mastertech.cartao.models.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class AlterarCartaoDTOEntrada {

    @NotNull
    @NotBlank
    private String numero;

    @NotNull
    private boolean ativo;

    public AlterarCartaoDTOEntrada() {
    }

    public AlterarCartaoDTOEntrada(String numero, boolean ativo) {
        this.numero = numero;
        this.ativo = ativo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
