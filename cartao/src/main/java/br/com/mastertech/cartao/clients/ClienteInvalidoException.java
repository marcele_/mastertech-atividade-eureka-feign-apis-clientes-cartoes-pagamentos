package br.com.mastertech.cartao.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "O cliente informado é inválido!")
public class ClienteInvalidoException extends RuntimeException {

}
