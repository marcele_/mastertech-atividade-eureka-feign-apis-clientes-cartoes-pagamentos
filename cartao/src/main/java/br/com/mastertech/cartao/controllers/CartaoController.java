package br.com.mastertech.cartao.controllers;

import br.com.mastertech.cartao.models.Cartao;
import br.com.mastertech.cartao.models.dtos.*;
import br.com.mastertech.cartao.security.principal.Usuario;
import br.com.mastertech.cartao.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private CartaoMapper mapper;

    @GetMapping("/cartoes")
    public Iterable<Cartao> listar(){
        try {
            return cartaoService.listarTodos();
        }catch (Exception ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @GetMapping("/cartao/{id}")
    public CartaoDTOSaida buscarPorId(@Valid @PathVariable(name = "id") int id) {
        try {
            Cartao cartao = cartaoService.buscarPorId(id);

            return mapper.paraCartaoDTOSaida(cartao);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @GetMapping("/cartao/n/{numero}")
    public CartaoDTOSaidaAtivoOculto buscarPorNumero(@Valid @PathVariable(name = "numero") String numero){
        try{
            Cartao cartao = cartaoService.buscarPorNumero(numero);

            return mapper.paraCartaoDTOSaidaOculto(cartao);

        }catch (Exception ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PostMapping("/cartao")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CartaoDTOSaida> cadastrar(@RequestBody @Valid CartaoDTOEntrada cartaoDTOEntrada,
                                                    @AuthenticationPrincipal Usuario usuario){
            Cartao cartao = mapper.paraCartao(cartaoDTOEntrada);
            cartao = cartaoService.salvar(cartao);
            CartaoDTOSaida cartaoDTOSaida = mapper.paraCartaoDTOSaida(cartao);
            return ResponseEntity.status(201).body(cartaoDTOSaida);
    }

    @PatchMapping("/cartao/{numero}")
    public CartaoDTOSaida alteraStatusAtivo(@Valid @PathVariable(name = "numero" ) String numero, @RequestBody AlterarCartaoDTOEntrada alterarCartaoDTOEntrada){
        try{
            alterarCartaoDTOEntrada.setNumero(numero);
            Cartao cartaoRecebido = mapper.paraCartao(alterarCartaoDTOEntrada);

            Cartao cartao = cartaoService.alteraStatusAtivo(cartaoRecebido);

            CartaoDTOSaida cartaoDTOSaida = mapper.paraCartaoDTOSaida(cartao);

            return cartaoDTOSaida;
        }catch (Exception ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }
}
