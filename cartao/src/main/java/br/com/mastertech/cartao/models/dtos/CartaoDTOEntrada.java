package br.com.mastertech.cartao.models.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CartaoDTOEntrada {
    @NotNull
    @NotBlank
    private String numero;

    @NotNull
    private int clienteId;

    public CartaoDTOEntrada() {
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }
}
