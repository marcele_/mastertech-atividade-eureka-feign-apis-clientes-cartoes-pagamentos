package br.com.mastertech.cartao.models.dtos;

import br.com.mastertech.cartao.models.Cartao;
import br.com.mastertech.cartao.models.Cliente;
import org.springframework.stereotype.Component;

@Component
public class CartaoMapper {

    public Cartao paraCartao(CartaoDTOEntrada cartaoDTOEntrada){
        Cartao cartao = new Cartao(cartaoDTOEntrada.getNumero(), false, cartaoDTOEntrada.getClienteId());

        return cartao;
    }

    public Cartao paraCartao(AlterarCartaoDTOEntrada alterarCartaoDTOEntrada){
        Cartao cartao = new Cartao();
        cartao.setNumero(alterarCartaoDTOEntrada.getNumero());
        cartao.setAtivo(alterarCartaoDTOEntrada.isAtivo());
        return cartao;
    }

    public CartaoDTOSaida paraCartaoDTOSaida(Cartao cartao){
        CartaoDTOSaida cartaoDTOSaida = new CartaoDTOSaida(
                cartao.getId(), cartao.getNumero(), cartao.getIdCliente(), cartao.isAtivo());

        return cartaoDTOSaida;
    }

    public CartaoDTOSaidaAtivoOculto paraCartaoDTOSaidaOculto(Cartao cartao){
        CartaoDTOSaidaAtivoOculto cartaoDTOSaidaAtivoOculto = new CartaoDTOSaidaAtivoOculto(
                cartao.getId(), cartao.getNumero(), cartao.getIdCliente());
        return cartaoDTOSaidaAtivoOculto;
    }

}
