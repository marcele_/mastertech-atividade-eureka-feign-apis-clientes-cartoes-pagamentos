package br.com.mastertech.cartao.clients;

import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            return new ClienteInvalidoException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}
