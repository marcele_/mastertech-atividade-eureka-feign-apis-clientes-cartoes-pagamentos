package br.com.mastertech.cartao.services;

import br.com.mastertech.cartao.clients.ClienteClient;
import br.com.mastertech.cartao.models.Cartao;
import br.com.mastertech.cartao.models.Cliente;
import br.com.mastertech.cartao.models.dtos.CartaoDTOEntrada;
import br.com.mastertech.cartao.models.dtos.CartaoDTOSaida;
import br.com.mastertech.cartao.models.dtos.CartaoDTOSaidaAtivoOculto;
import br.com.mastertech.cartao.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteClient clienteClient;

    public Iterable<Cartao> listarTodos(){
        return cartaoRepository.findAll();
    }

    public Cartao buscarPorId(int id){
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(id);

        if (cartaoOptional.isPresent()){
            return cartaoOptional.get();
        } else {
            throw new RuntimeException("O cartão com id: "+ id + " não foi encontrado.");
        }
    }

    public Cartao buscarPorNumero(String numero){
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);
        if (cartaoOptional.isPresent()){
            return cartaoOptional.get();
        } else {
            throw new RuntimeException("O cartão nº "+ numero +" não foi encontrado!");
        }
    }

    public Cartao salvar(Cartao cartao){
        Optional<Cliente> clienteOptional = clienteClient.buscarPorId(cartao.getIdCliente());

        if (clienteOptional.isPresent()){
            Cartao cartaoSalvo = cartaoRepository.save(cartao);
            return cartaoSalvo;
        }
        else
        {
            throw new RuntimeException("O cliente "+ cartao.getIdCliente() +" não foi encontrado!");
        }
    }

    public Cartao alteraStatusAtivo(Cartao cartao){
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(cartao.getNumero());
        if (cartaoOptional.isPresent()){
            cartaoOptional.get().setAtivo(cartao.isAtivo());
            return cartaoRepository.save(cartaoOptional.get());
        } else {
            throw new RuntimeException("O cartão " + cartao.getNumero() + "  não foi encontrado!");
        }
    }

}
